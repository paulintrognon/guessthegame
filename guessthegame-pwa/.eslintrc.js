module.exports = {
  root: true,
  overrides: [
    // SOURCE TSX FILES
    {
      files: ['src/**/*.tsx'],
      parser: '@typescript-eslint/parser',
      parserOptions: {
        ecmaFeatures: { jsx: true },
      },
      env: {
        es6: true,
        browser: true,
        node: true,
      },
      extends: [
        'eslint:recommended',
        'plugin:@typescript-eslint/eslint-recommended',
        'plugin:@typescript-eslint/recommended',
        'plugin:react/recommended',
        'plugin:jsx-a11y/recommended',
        'prettier/@typescript-eslint',
        'plugin:prettier/recommended',
      ],
      rules: {
        '@typescript-eslint/explicit-function-return-type': [
          'warn',
          {
            allowExpressions: true,
            allowTypedFunctionExpressions: true,
            allowHigherOrderFunctions: true,
            allowConciseArrowFunctionExpressionsStartingWithVoid: true,
          },
        ],
        'prettier/prettier': ['error', {}, { usePrettierrc: true }],
        'react/prop-types': 'off',
        'react/react-in-jsx-scope': 'off',
        'no-use-before-define': ['error', { functions: true, classes: true }],
        'no-console': [2, { allow: ['debug', 'warn', 'error'] }],
      },
    },
    // CYPRESS TEST FILES
    {
      files: ['cypress/**/*.js'],
      plugins: ['cypress'],
      extends: ['eslint:recommended', 'plugin:prettier/recommended', 'plugin:cypress/recommended'],
      env: {
        es6: true,
        node: true,
      },
      rules: {
        'prettier/prettier': ['error', {}, { usePrettierrc: true }],
      },
    },
  ],
}
